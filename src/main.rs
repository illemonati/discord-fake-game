
use discord_game_sdk::{Discord, EventHandler};
use discord_game_sdk::Activity;
use std::time;
use chrono::prelude::*;
use chrono::offset::LocalResult;

const DISCORD_CLIENT_ID: i64 = 767804459719589898;


struct FakeGameEventHandler {

}

impl EventHandler for FakeGameEventHandler {

}


fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut discord = Discord::new(DISCORD_CLIENT_ID).expect("Discord Creation Error");
    *discord.event_handler_mut() = Some(FakeGameEventHandler{});
    
    loop {
        discord.run_callbacks()?;
        let time_started = Local::now() + chrono::Duration::hours(-12) + chrono::Duration::minutes(-12)+ chrono::Duration::seconds(-12);
        // let time_ended = Local::now() + chrono::Duration::hours(24) + chrono::Duration::minutes(6) + chrono::Duration::seconds(7);
        discord.update_activity(
            &Activity::empty()
                .with_details("The BIGGEE CHEESE")
                .with_state("Eating Cheese")
                .with_large_image_key("chuckecheese")
                .with_small_image_key("cheese4")
                .with_large_image_tooltip("ChEEsE")
                .with_small_image_tooltip("CHeeSE")
                .with_party_id("GAMER")
                .with_party_amount(69)
                .with_party_capacity(69*2)
                .with_join_secret("ChEEsE")
                .with_spectate_secret("CHEESe")
                .with_match_secret("ChEESe")
                .with_start_time(time_started.timestamp_millis() as i64)
                // .with_end_time(time_ended.timestamp_millis() as i64)
                ,
            |discord, result| {
                if let Err(error) = result {
                    eprintln!("failed to update activity: {}", error);
                }
            },
        );
        std::thread::yield_now();
        std::thread::sleep(std::time::Duration::from_secs(1));
    }
    Ok(())
}